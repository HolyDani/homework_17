#include <iostream>
#include<cmath>

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}

	void SetPoint()
	{
		std::cout << "x = ";
		std::cin >> x;
		std::cout << "y = ";
		std::cin >> y;
		std::cout << "z = ";
		std::cin >> z;
	}

	void GetPoint()
	{
		std::cout << "x = " << x << "\ty = " << y << "\tz = " << z << std::endl;
	}

	double VectorModulus()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

private:
	double x, y, z;
};


class MyClass
{
public:
	MyClass() : a(0), b(0)
	{}

	MyClass(int a, int b)
	{
		this->a = a;
		this->b = b;
	}

	void GetInfo()
	{
		std::cout << "a = " << a << "\tb = " << b << std::endl;
	}

private:
	int a, b;
};

int main()
{
	Vector PointA;
	std::cout << "Setup values:" << std::endl;
	PointA.SetPoint();
	std::cout << "\n";
	std::cout << "Get values:" << std::endl;
	PointA.GetPoint();
	std::cout << "\n";
	std::cout<<"Calculating modulo vector:"<< std::endl;
	std::cout << PointA.VectorModulus() << std::endl << std::endl;
	
	std::cout << "========================" << std::endl;
	std::cout << "========================" << std::endl << std::endl;

	MyClass Point;
	MyClass Point1(1, 2);
	Point.GetInfo();
	Point1.GetInfo();

	return 0;
}